# Wardrobify

Team:

* John Jackson - Hats microservice
* John Liu - Shoes microservice

## Design

## Shoes microservice

The models include two classes. One for Shoes and the other for BinVO.
    (1) Shoes have 4 fields(manufacture, model, color, and picture_url) plus one foreign key as bin.
    (2) BinVO (bin value-object) has import_href, closet_name, bin_number and bin_size. Those are polled via api from the Wardrobe to be further stored in the database.

As mentioned for the polling, I used poll.py to transmit data from Wardrobe to the BinVO model. It also serves as anti-corruption layer in between so that I can filter the data that I need to be actually stored into our db for the shoes microservices, although in this case, I stored all the data from Wardrobe except the id.

After that, I created three methods for restful API as "GET", "POST" and "DELETE" functions and add corresponding url in Django app - shoes_rest and Django project - shoes_project. To be sure everything I need from the backend is working, I checked via insomnia to get correct data in JSON.

For the frontend, I used React to render the webpage with the data fetching from the api along with the deletion thereof.


## Hats microservice

MODELS:
I created my LocationVo model to pull all the data from the location model in the wardrobe and included the import_href to hold the api href to reference when creating my views, poller, and react components. I have the locations being referenced by the closet name with str self function

I created the hat model with the relevant data given to us in learn and made the location foreign key to reference the LocationVo model, to have the location data be accessible by the hat model. The hats are being referenced to by the style name. I also have a get_api_url method so the model encoder will add and href value of a url so the I could access int through the api.

VIEWS:
I originally created 3 encoders for my view functions, LocationVODetailEncoder, HatListEncoder, and HatDetailEncoder, but as the project went on I didn't see a use for the HatListEncoder so I commented it out. While I was working on the react components I realized I could either pull data from my detail api and my list api, add more properties to my list encoder which would end up looking a lot like the detail encoder or just use the detail encoder and save some time. 

I created 2 view functions, api_list_hats that returns the list of hats with a GET method and creates a hat with the POST method. Originally I had the GET method feeding the listEncoder but switched it to the DetailEncoder because it was a better fit once I started to work on the react components.

The api_show_hats function returns a detail view of a single hat and also deletes a hat.

POLLER:
From the wardrobe api I'm polling a list of locations and updating the LocationVO objects either by updating a current object or creating a new one. The poller either gets the location object back or returns an error. 

REACT:
My partner finished his backend before I did so he setup the majority of the App.js with exception of the Routes to the hat side of the project.

I started out making the HatList.js first, which shows a list of all of the hats in a list with all of the features of the specific hats in that list along with a button to delete a specific hat on that hats list row. I have 2 functions in there, the HatList which pull data from the list api I created and the deleteHat function which uses my delete api and the id from the hat that is being deleted. I added a warning because I'm anxious about things being deleted and the then after it is deleted it refershes the page.

In the AddHatsForm.js I populated the drop down menue for the locations by using the list locations api. I then set all the hat objects to a use state and created the handleChange's so the form could hold the data into it at real time. Then created the handle submit to create a new hat object once onSubmit once the new hat is created it resets the form back to blank