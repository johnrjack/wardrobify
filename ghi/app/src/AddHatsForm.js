import React, { useState, useEffect } from "react";
import { resolvePath } from "react-router-dom";

function AddHatsForm() {
    // create drop down of locations
    const [locations, setLocations] = useState([]);
    const fetchData = async () => {
        const locationsUrl = "http://localhost:8100/api/locations/";
        const response = await fetch(locationsUrl);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
    }

    // set useState and create handlers
    const [fabric, setFabric] = useState('');
    const [styleName, setStyleName] = useState('');
    const [color, setColor] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [location, setLocation] = useState('');
    
    const handleFabricChange = (event) => {
        const value = (event.target.value);
        setFabric(value);
    }

    const handleStyleNameChange = (event) => {
        const value = (event.target.value);
        setStyleName(value);
    }

    const handleColorChange = (event) => {
        const value = (event.target.value);
        setColor(value);
    }

    const handlePictureUrlChange = (event) => {
        const value = (event.target.value);
        setPictureUrl(value);
    }

    const handleLocationChange = (event) => {
        const value = (event.target.value);
        setLocation(value);
    }
    
    // create submit functionality
    const handleSubmit = async (event) => {
        event.preventDefault();

        const data= {}

        data.fabric = fabric;
        data.style_name = styleName;
        data.color = color;
        data.picture_url = pictureUrl;
        data.location = location;

        const hatUrl = `http://localhost:8090${location}hats/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);

            setFabric('');
            setStyleName('');
            setColor('');
            setPictureUrl('');
            setLocation('');
            window.location.href = 'http://localhost:3000/hats/';
        };
    }
    useEffect(() =>{
        fetchData();
    }, []);

    
return(
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add New Hats</h1>
            <form onSubmit={handleSubmit} id="create-hats-form">

              <div className="form-floating mb-3">
                <input onChange={handleFabricChange} value={fabric} placeholder="fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                <label htmlFor="fabric">Fabric</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handleStyleNameChange} value={styleName} placeholder="styleName" required type="text" name="styleName" id="styleName" className="form-control" />
                <label htmlFor="styleName">Style Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColorChange} value={color} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePictureUrlChange} value={pictureUrl} placeholder="pictureUrl" required type="text" name="picture_url" id="pictureUrl" className="form-control" />
                <label htmlFor="picture Url">Picture Url</label>
              </div>

              <div className="mb-3">
                <select onChange={handleLocationChange} value={location} required id="location" name="location" className="form-select">
                  <option value="">Choose a Location</option>
                  {locations.map(location => {
                                    return (
                                        <option key={location.href} value={location.href}>{location.closet_name}</option>
                                    )
                                })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
  );
}


export default AddHatsForm;