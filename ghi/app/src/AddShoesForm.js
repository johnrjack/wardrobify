import React, {useEffect, useState} from 'react';


function AddShoesForm( ) {
  const [bins, setBins] = useState([]);
  const fetchData = async () => {
    const url = 'http://localhost:8100/api/bins/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      console.log(data)
      setBins(data.bins)
    }
  }
  useEffect(() => {
    fetchData();
  }, []);

  const [manufacturer, setManufacturer] = useState('');
  const [model, setModel] = useState('');
  const [color, setColor] = useState('');
  const [pictureUrl, setPictureUrl] = useState('');
  const [bin, setBin] = useState('');

  const handleManufacturerChange = (event) => {
    const value = event.target.value;
    setManufacturer(value);
  }
  const handleModelChange = (event) => {
    const value = event.target.value;
    setModel(value);
  }
  const handleColorChange = (event) => {
    const value = event.target.value;
    setColor(value);
  }
  const handlePictureUrlChange = (event) => {
    const value = event.target.value;
    setPictureUrl(value);
  }
  const handleBinChange = (event) => {
    const value = event.target.value;
    setBin(value);
  }



  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.manufacturer = manufacturer;
    data.model = model;
    data.color = color;
    data.picture_url = pictureUrl;
    data.bin = bin;

    const BinUrl = `http://localhost:8080${bin}shoes/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const ShoesResponse = await fetch(BinUrl, fetchConfig);
    if (ShoesResponse.ok) {
      setManufacturer('');
      setModel('');
      setColor('');
      setPictureUrl('');
      setBin('');
      window.location.href = 'http://localhost:3000/shoes/';
    }
  }


  return(
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add New Shoes</h1>
            <form onSubmit={handleSubmit} id="create-shoes-form">

              <div className="form-floating mb-3">
                <input onChange={handleManufacturerChange} value={manufacturer} placeholder="manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handleModelChange} value={model} placeholder="model" required type="text" name="model" id="model" className="form-control" />
                <label htmlFor="model">Model</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handleColorChange} value={color} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>


              <div className="form-floating mb-3">
                <input onChange={handlePictureUrlChange} value={pictureUrl} placeholder="pictureUrl" required type="text" name="picture_url" id="pictureUrl" className="form-control align-middle" />
                <label htmlFor="picture Url">Picture Url</label>
              </div>

              <div className="mb-3">
                <select onChange={handleBinChange} value={bin} required id="bin" name="bin" className="form-select">
                  <option value="">Choose a bin</option>

                  {bins.map((bin) => {
                          return (
                            <option value={bin.href} key={bin.href}>
                              Closet: {bin.closet_name} - Bin# {bin.bin_number}
                            </option>
                          );
                        })}

                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
  );
}


export default AddShoesForm;
