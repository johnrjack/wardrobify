import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList';
import AddShoesForm from './AddShoesForm'
import HatsList from './HatList';
import AddHatsForm from './AddHatsForm';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
            <Route path="shoes">
              <Route path="" element={<ShoesList />} />
              <Route path="new" element={<AddShoesForm />} />
            </Route>
            <Route path="hats">
              <Route path="" element={<HatsList/>} />
              <Route path="new" element={<AddHatsForm/>}/>
            </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
