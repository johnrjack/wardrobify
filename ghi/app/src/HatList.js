import React, {useEffect, useState} from "react";
import { NavLink } from "react-router-dom";

function HatsList(){
    const [hats, setHats] = useState([]);
    const fetchData = async () => {
        const listUrl = 'http://localhost:8090/api/hats/';
        const response = await fetch(listUrl);

        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setHats(data.hats)
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    function deleteHat(id){
        fetch(`http://localhost:8090/api/hats/${id}`,{
            method: 'DELETE'
        }).then((result) =>{
            result.json().then((deleteResponse) =>{
                console.warn(deleteResponse)
            })
            return window.location.href = "http://localhost:3000/hats"
        })
    }

return(
    <>
    <h1>My Hats</h1>
    <table className="table table-striped">
        <thead>
        <tr>
            <th>Fabric</th>
            <th>Style Name</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Closet Name</th>
            <th>Section Number</th>
            <th>Shelf Number</th>
            <th>Deletion</th>
        </tr>
        </thead>
        <tbody>

        {hats.map((hats, index) => {
            return (
            <tr key={ index }>
                <td>{ hats.fabric }</td>
                <td>{ hats.style_name }</td>
                <td>{ hats.color }</td>
                <td><img src={hats.picture_url} alt="" height="100" width="100"></img></td>
                <td>{ hats.location.closet_name}</td>
                <td>{ hats.location.section_number}</td>
                <td>{ hats.location.shelf_number}</td>
                <td className="align-middle">
                    <button onClick={() =>deleteHat(hats.id)} type="button" className="btn btn-danger" >Delete</button>
                </td>
            </tr>
            );
        })}
        </tbody>
        </table>
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <NavLink to="/hats/new" className="btn btn-primary btn-lg px-4 gap-3">Add New Hats</NavLink>
        </div>
    </>

  );
}


export default HatsList;
