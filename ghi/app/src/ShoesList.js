import React, {useEffect, useState} from 'react';
import { NavLink } from 'react-router-dom';

function ShoesList( ) {
  const [shoes, setShoes] = useState([]);
  const fetchData = async () => {
    const url = 'http://localhost:8080/api/shoes/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      console.log(data)
      setShoes(data.shoes)
    }

  }
  useEffect(() => {
    fetchData();
  }, []);


  function deleteShoe(id) {
    fetch(`http://localhost:8080/api/shoes/${id}`, {
      method:'DELETE'
    }).then(()=>{
        return window.location.href = 'http://localhost:3000/shoes/';
    });
  }

  return(
    <>
    <h1>My Shoes</h1>
    <table className="table table-striped">
        <thead>
        <tr>
            <th>Manufacturer</th>
            <th>Model</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Closet Name</th>
            <th>Bin Number</th>
            <th>Bin Size</th>
            <th>Deletion</th>
        </tr>
        </thead>
        <tbody>

        {shoes.map((shoes) => {
            return (
            <tr key={ shoes.id }>
                <td className="align-middle">{ shoes.manufacturer }</td>
                <td className="align-middle">{ shoes.model }</td>
                <td className="align-middle">{ shoes.color }</td>
                <td className="align-middle"><img src={shoes.picture_url} alt="" height="100" width="100"></img></td>
                <td className="align-middle">{ shoes.bin.closet_name }</td>
                <td className="align-middle">{ shoes.bin.bin_number }</td>
                <td className="align-middle">{ shoes.bin.bin_size }</td>
                <td className="align-middle">
                    <button type="button" className="btn btn-danger" onClick={() => deleteShoe(shoes.id)}>Delete</button>
                </td>
            </tr>
            );
        })}
        </tbody>
        </table>
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <NavLink to="/shoes/new" className="btn btn-primary btn-lg px-4 gap-3">Add New Shoes</NavLink>
        </div>
    </>

  );
}


export default ShoesList;
