from django.contrib import admin
from .models import Shoes, BinVO
# Register your models here.


@admin.register(Shoes)
class ShoesAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "manufacturer",
        "model",
        "color",
        "picture_url",
        "bin",
    )

@admin.register(BinVO)
class ShoesAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "import_href",
        "closet_name",
        "bin_number",
        "bin_size",
    )
